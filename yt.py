import os
from pytube import YouTube

link=input("Enter link here: ")
yt=YouTube(link)

#Title of video
print("Titile: ", yt.title)
print()
# Number of views of video
print("Number if views: ", yt.views) 
print()
#Length of the video
print("Length of video: ", yt.length, "seconds")
print()
# #Description of video
print("Description: ", yt.description)
print()
# A list of streams
for s in yt.streams:
    print(s)

print()
# Download audio by itag 
print("Download starting......")
ys=yt.streams.get_by_itag(input("Enter audio by itag: ")).download()
# Put orginal video name into a dictionary
d={}
d["ys"]=ys
# Change audio name to audio.mp4
os.rename(ys, "audio.mp4")   
# Download video by itag
yz=yt.streams.get_by_itag(input("Enter video by itag: ")).download()
# Change video name to video.mp4
os.rename(yz,"video.mp4")
# Combine audio.mp4 and video.mp4
os.system("ffmpeg -i video.mp4 -i audio.mp4 -c:v copy -map 0:v:0 -map 1:a:0 output.mp4")
# Delete audio,mp4 and video.mp4
os.remove("audio.mp4")
os.remove("video.mp4")
# Rename the finished product back to the original name
os.rename("output.mp4", d["ys"])

